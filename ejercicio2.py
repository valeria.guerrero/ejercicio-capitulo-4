# ---autora---= "Valeria Guerrero"
# ---email--- = "valeria.guerrero@unl.edu.ec"
# Desplaza la última línea del programa anterior hacia arriba,
# de modo que la llamada a la función aparezca antes que las definiciones.
# Ejecuta el programa y observa qué mensaje de error obtienes.

def muestra_estribillo():
    print("Soy un profesor, que felicidad")
    print("Trabajo por el día y en la noche descanso")


def repite_estribillo():
    muestra_estribillo()
    muestra_estribillo()
    muestra_estribillo()
repite_estribillo()
print("Soy un profesor, que felicidad")
print("Trabajo por el día, y en la noche descanso")


#presenta los mensajes de la función repite_estribillo, que se encuentra en la función
#muestra_estribillo y las sentencias sentencias de la función muestra_estribillo
#sin llamar a la función.



