# ---autora---= "Valeria Guerrero"
# ---email--- = "valeria.guerrero@unl.edu.ec"
# Reescribe el programa de cálculo del salario, con tarifa-ymedia
# para las horas extras, y crea una función llamada calculo_salario
# que reciba dos parámetros (horas y tarifa).


def  calculo_salario(horas,tarifahora):
    if horas>40:
        horasextra= horas - 40
        print("El número de horas extras trabajadas es:", horasextra)
        salarios= (horas - horasextra) * tarifahora
        print("El salario normal es" , salarios)
        tarifaextra= (tarifahora * 1.5) * horasextra
        print("El valor por horas extras es:" , tarifaextra)

        salario = tarifaextra + salarios
        print("El salario final es" , salario)
    elif horas <=40:
        salario = horas*tarifahora
        print("El salario final es", salario)
        return salario


if __name__ == '__main__':
    while True:
        try:
            horas = int(input("ingrese el numero de horas laboradas: \n"))
            while True:
                try:
                    tarifahora = float(input("ingrese la tarifa por hora: \n "))
                    salario = calculo_salario(horas, tarifahora)
                    print('Su salario es de: ', salario)
                    break
                except ValueError:
                    print("Tarifa incorrecta: Ingrese numeros")
            break
        except ValueError:
            print("Hora incorrecta: Ingrese numeros")