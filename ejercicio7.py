# ---autora---= "Valeria Guerrero"
# ---email--- = "valeria.guerrero@unl.edu.ec"

# Reescribe el programa de calificaciones del capítulo anterior
# usando una función llamada calcula_calificacion, que reciba una
# puntuación como parámetro y devuelva una calificación como cadena.

# Puntuación Calificación
# > 0.9 Sobresaliente
# > 0.8 Notable
# > 0.7 Bien
# > 0.6 Suficiente
# <= 0.6 Insuficiente



def calcula_calificacion(cal):
    if cal>=0.9 and cal<=1.0:
        print("sobresaliente")
    elif cal>=0.8 and cal<0.9:
        print("notable")
    elif cal>=0.7 and cal<0.8:
        print("bien")
    elif cal>=0.6 and cal<0.7:
        print("suficiente")
    elif cal <0.6:
        print("insuficiente")
    else:
        print("La puntuación no es correcta")
        return cal


if __name__ == '__main__':
    while True:
        try:
            p = float(input('Introduzca la puntuacion '))
            print(calcula_calificacion(p))
            break
        except ValueError:
            print('Puntuacion incorrecta, ingrese numeros')
