# ---autora---= "Valeria Guerrero"
# ---email--- = "valeria.guerrero@unl.edu.ec"

# ¿Qué mostrará en pantalla el siguiente programa Python?

def fred():
    print("Zap")

def jane():
    print("ABC")

jane()
fred()
jane()

print("La respuesta correcta es el literal d: ABC Zap ABC")

