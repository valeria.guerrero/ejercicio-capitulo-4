# ---autora---= "Valeria Guerrero"
# ---email--- = "valeria.guerrero@unl.edu.ec"

# Desplaza la llamada de la función de nuevo hacia el final,
# y coloca la definición de muestra_estribillo después de la definición de
# repite_estribillo. ¿Qué ocurre cuando haces funcionar ese programa
        #Ejercicio 2
def muestra_estribillo():
    print("Soy un profesor, que felicidad")
    print("Trabajo por el día y en la noche descanso")
    def repite_estribillo():
        muestra_estribillo()
        muestra_estribillo()
        muestra_estribillo()

        #Ejercicio 3

print("Soy un profesor, que felicidad")
print("Trabajo por el día y en la noche descanso")
def repite_estribillo():
    def muestra_estribillo():
        muestra_estribillo()
        muestra_estribillo()
        muestra_estribillo()
repite_estribillo()


